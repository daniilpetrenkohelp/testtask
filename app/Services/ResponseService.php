<?php
namespace App\Services;

use Response;
use Log;
use App\Models\Task;

class ResponseService{
	public function responseWithResults($task){
		Log::debug('response with results');
		return Response::json([
			'status' => 'ready',
			'result' => $task->result,
		], 200);
	}
	public function responseWithReceived($task){
		Log::debug('response with received');
		return Response::json([
			'status' => 'received',
			'result' => 'none'
		], 200);
	}
	public function responseWithError(){
		Log::debug('response with error');
		return Response::json([
			'status' => 'error',
		], 500);
	}
	public function responseWithWait($task){
		Log::debug('response with wait');
		return Response::json([
			'status' => 'wait',
			'result' => 'none'
		], 200);
	}
}