<?php
namespace App\Services;

use App\Services\SendDataToApiService;
use App\Services\ResponseService;

use App\Models\Task;
use App\Models\ProcessedPhoto;
use App\Models\SentData;

use App\Jobs\ProcessData;

class ProcessingService{
	
	private $sendDataToApiService;
	private $responseService;
	public function __construct(
		SendDataToApiService $sendDataToApiService,
		ResponseService $responseService,
	){
		$this->sendDataToApiService = $sendDataToApiService;
		$this->responseService = $responseService;
	}

	public function startProcessing($task, $photo, $name){
		$taskId = $task->id;
		
		$processingStatus = $this->findProcessingStatus($taskId);
		if($processingStatus == null){
			//Send data to API
			$data = array(
				'photo' => $photo,
				'name' => $name
			);
			$result = $this->sendDataToApiService->sendData($data);
			$result = json_decode($result);
			if($result->status == 'success'){
				$this->updateTask($task, $result->result);
				//Responce with success status
				return $this->responseService->responseWithResults($task);
			}elseif($result->status == 'wait'){
				//Creating processing status for a job
				$retryId = $result->retry_id;
				$processingStatus = $this->newProcessingStatus($taskId, $retryId);
				ProcessData::dispatch($processingStatus);
				//Responce with received status
				return $this->responseService->responseWithReceived($task);
			}else{
				//EXCEPTION
				return $this->responseService->responseWithError();
			}
			
		}

		
	}
	private function findProcessingStatus($taskId){
		$status = SentData::where('task_id', '=', $taskId)->first();
		return $status;
	}
	private function newProcessingStatus($taskId, $retryId){
		$status = new SentData;
		$status->task_id = $taskId;
		$status->retry_id = $retryId;
		$status->active = true;
		$statusId = $status->save();
		return SentData::where('id', '=', $statusId)->first();
	}
	private function updateTask($task, $result){
		$task->completed = true;
		$task->result = $result;
		$task->save();
	}
}