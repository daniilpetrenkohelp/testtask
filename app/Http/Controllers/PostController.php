<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FindTaskService;
use App\Services\NewTaskService;
use App\Services\ResponseService;
use App\Services\ProcessingService;

class PostController extends Controller
{
    private $findTaskService;
    private $newTaskService;
    private $responseService;
    private $processingService;
    public function __construct(
        FindTaskService $findTaskService,
        NewTaskService $newTaskService,
        ResponseService $responseService,
        ProcessingService $processingService,
    ){
        $this->findTaskService = $findTaskService;
        $this->newTaskService = $newTaskService;
        $this->responseService = $responseService;
        $this->processingService = $processingService;
    }
    public function getRequest(Request $request){
        $name = $request->input('name');
        $photo = $request->file('photo');
        $photoHash = $this->getHashFile($photo);
        $task = $this->getTask($photoHash);
        if($task->completed == true){
            return $this->responseService->responseWithResults($task);
        }else{
            return $this->processingService->startProcessing($task, $photo, $name);
        }
    }


    private function getHashFile($file){
        return md5_file($file->path());
    }
    private function getTask($hash){
        $task = $this->findTaskService->findTask($hash);
        if($task != null){
            return $task;
        }else{
            return $this->newTaskService->createTask($hash);
        }
    }
}
