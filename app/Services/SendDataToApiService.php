<?php
namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;

class SendDataToApiService{

	public function sendData($data){
		$url = env('API_URL');
		$filePath = $data['photo']->path();
		$fileData = curl_file_create($filePath, 'image/jpeg', 'image');
		$nameData = $data['name'];
		$dataForApi = array(
			'photo' => $fileData,
			'name' => $nameData
		);
		$apiConnect = curl_init();
		curl_setopt_array($apiConnect, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => $dataForApi,
		));
		$result = curl_exec($apiConnect);
		Log::debug($result);
		return $result;
	}
	public function getConnect($retryId){
		$url = env('API_URL');
		$apiConnect = curl_init();
		curl_setopt_array($apiConnect, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('retry_id' => $retryId),
		));
		$result = curl_exec($apiConnect);
		Log::debug($result);
		return $result;
	}
}