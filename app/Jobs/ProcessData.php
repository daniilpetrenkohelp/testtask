<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Services\SendDataToApiService;
use App\Services\ResponseService;

use App\Models\Task;
use App\Models\SentData;
class ProcessData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $sendDataToApiService;
    private $responseService;
    public function __construct(
        SendDataToApiService $sendDataToApiService,
        ResponseService $responseService
    )
    {
        $this->sendDataToApiService = $sendDataToApiService;
        $this->responseService = $responceService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle($processingStatus)
    {
        $retryId = $processingStatus->retry_id;
        $result = $this->sendDataToApiService->getConnect($retryId);
        $result = json_decode($result);
        if($result->status == 'success'){
            $processingStatus->active = false;
            $processingStatus->save();
            $task = Task::where('id', '=', $processingStatus->task_id);
            $task->completed = true;
            $task->result = $result->result;
        }
    }
}
