<?php
namespace App\Services;

use App\Models\Task;
use App\Models\ProcessedPhoto;

class FindTaskService{
	function findTask($hash){
		$photo = $this->findProcessedPhoto($hash);
		if($photo == null){
			return null;
		}else{
			return Task::where('photo_id', '=', $photo->id)->first();
		}
	}
	function findProcessedPhoto($hash){
		return ProcessedPhoto::where('hash', '=', $hash)->first();
	}
}