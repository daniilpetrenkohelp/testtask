<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Task;

use App\Services\ResponseService;
use App\Services\FindTaskService;

class GetController extends Controller
{
    private $responseService;
    public function __construct(
        ResponseService $responseService,
    ){
        $this->responseService = $responceService;
    }
    public function showTask(Request $request){
        $taskId = $request->input('task_id');
        $task = Task::where('id', '=', $taskId)->first();
        if($task == null){
            return $this->responseService->responseWithNotFound();
        }else{
            if($task->completed == true){
                return $this->responseService->responseWithResults($task);
            }else{
                return $this->responseService->responseWithWait($task);
            }
        }
    }
}
