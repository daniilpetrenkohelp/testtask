<?php
namespace App\Services;

use App\Models\Task;
use App\Models\ProcessedPhoto;

class NewTaskService{

	public function createTask($hash){
		$photoId = $this->createProcessedPhoto($hash);
		$task = new Task;
		$task->photo_id = $photoId;
		$task->result = 0;
		$task->completed = false;
		$task->save();
		$taskId = $task->id;
		return Task::where('id', '=', $taskId)->first();
	}
	private function createProcessedPhoto($hash){
		$photo = new ProcessedPhoto;
		$photo->hash = $hash;
		$photo->save();
		return $photo->id;
	}
}